app.controller('PengambilanFotoCtrl', function($scope, $http, $cordovaGeolocation, $ionicModal, $cordovaFileTransfer, $cordovaCamera, $q){

    $scope.listPekerjaan    = [];

    $scope.getListPekerjaan = function(){
        $http.get(BASE_URL+'project/list_pekerjaan', temp_config).success(function(res){
            $scope.listPekerjaan = res.data.titik;
            $scope.loadMarker($scope.listPekerjaan);
        });
    }

    var options = {timeout: 10000, enableHighAccuracy: true};
    $cordovaGeolocation.getCurrentPosition(options).then(function(position){
 
        var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        // var latLng = new google.maps.LatLng(37.3960513, -122.1180187);
     
        var mapOptions = {
          center: latLng,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
     
        $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);

      }, function(error){
        console.log("Could not get location");
    });

    $scope.loadMarker = function(data){
        console.log(data);
        google.maps.event.addListenerOnce($scope.map, 'idle', function(){

            for(i=0; i < data.length; i++){
                var markerPos = new google.maps.LatLng(data[i].titikkoordinat.split(",")[0], data[i].titikkoordinat.split(",")[1]);

                var marker = new google.maps.Marker({
                    map: $scope.map,
                    animation: google.maps.Animation.DROP,
                    position: markerPos
                });     

                $scope.addInfoMarker(marker);
            }
            
        });
    }

    $scope.addInfoMarker = function(marker){
        // var infoWindow = new google.maps.InfoWindow({
        //     content: message
        // });
   
        google.maps.event.addListener(marker, 'click', function () {
            // infoWindow.open(map, marker);
            $scope.openModal();
        });  
    }

    $ionicModal.fromTemplateUrl('templates/form-pengambilanfoto.html', {
        scope: $scope,
        animation: 'slide-in-up',
    }).then(function(modal) {
        $scope.modal = modal;
    });
      
    $scope.openModal = function() {
        $scope.modal.show();
    };
      
    $scope.closeModal = function() {
        $scope.modal.hide();
    };
      
    //Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
        $scope.modal.remove();
    });
    
    // Execute action on hide modal
    $scope.$on('modal.hidden', function() {
        // Execute action
    });
      
    // Execute action on remove modal
    $scope.$on('modal.removed', function() {
        // Execute action
    });
    
    $scope.openCamera = function(){
		var defer = $q.defer();
		defer.promise.then(function (imageData){
			console.log(imageData);
			$scope.imageName    =  'camera_captured.jpg';
			var image		    = imageData;
			$scope.theimage	    = image;
			$scope.capturedImage = "data:image/jpeg;base64," + $scope.theimage;
			console.log($scope.capturedImage);
			$scope.images = $scope.capturedImage;
		}, function (error){
			console.log(error);
		});

		navigator.camera.getPicture(defer.resolve, defer.reject, {
			quality				: 50,
			destinationType		: Camera.DestinationType.DATA_URL,
			sourceType			: Camera.PictureSourceType.CAMERA,
			mediaType			: Camera.MediaType.PICTURE,
			correctOrientation	: true,
			encodingType		: Camera.EncodingType.JPEG,
			targetWidth			: 640,
			targetHeight		: 480,
			saveToPhotoAlbum	: true
		});
    }

    //init
    setTimeout(function() {
		$scope.getListPekerjaan();
	}, 1000);
    


});