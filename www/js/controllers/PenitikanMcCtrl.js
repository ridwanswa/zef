app.controller('PenitikanMcCtrl', function($scope, $http, $cordovaGeolocation, $ionicHistory, $ionicPopup){
    
    var dataPost = {};

    $scope.listProvinsi     = [];
    $scope.listProject      = [];
    $scope.listSegmen       = [];
    $scope.listItem         = [];
    $scope.listItemDetail   = [];
    $scope.lat              = '';
    $scope.long             = '';
    $scope.config_latlong   = {
        timeout: 10000, 
        enableHighAccuracy: false
    };
    $scope.listSimbol       = [];

    $scope.getListProjet = function(){
        $http.get(BASE_URL+'project/list', temp_config).success(function(res){
            $scope.listProject = res.data.project;
        });
    }

    $scope.getListProvinsi = function(idProject){
        $http.get(BASE_URL+'project/list_provinsi/'+idProject, temp_config).success(function(res){
            $scope.listProvinsi = res.data.project;
        });
    }

    $scope.getListSegmen = function(idProject, idProvinsi){
        $http.get(BASE_URL+'project/list_segmen/'+idProject+'/'+idProvinsi, temp_config).success(function(res){
            $scope.listSegmen = res.data.project;
        });
    }

    $scope.getListItem = function(idProject){
        $http.get(BASE_URL+'project/list_item/'+idProject, temp_config).success(function(res){
            $scope.listItem = res.data.project;
        });
    }

    $scope.getListItemDetail = function(idItem, idParent){
        $http.get(BASE_URL+'project/item_detail/'+idItem, temp_config).success(function(res){
            $scope.listItemDetail   = res.data;
            $scope.idParent         = idParent;
            $scope.idItem           = idItem;
        });
    }

    $scope.getListSimbol = function(parentId, type, category, name, option){
        $http.get(BASE_URL+'simbol/list/'+parentId+'/'+type+'/'+category, temp_config).success(function(res){
            $scope.listSimbol = res.data;
            $scope.a(name, option);
        });
    }

    $scope.test = function(){
        $cordovaGeolocation
        .getCurrentPosition($scope.config_latlong)
        .then(function (position) {
            $scope.lat  = position.coords.latitude
            $scope.long = position.coords.longitude
        }, function(err) {
            // error
        });
    }

    $scope.createTitik = function(idPekerjaan, jmlItem){
        dataPost['id_pekerjaan']    = idPekerjaan;
        dataPost['titikkoordinat']  = $scope.lat+','+$scope.long;
        dataPost['item']            = $scope.idItem;
        dataPost['subitem']         = '2';
        dataPost['jml_item']        = jmlItem;

        $http.post(BASE_URL+'titik/save', dataPost, temp_config).success(function(res){
            console.log(res);
            if(res.status == 'success'){
                $scope.showAlert(res.status, res.message);
            }
        })
    }

    $scope.a = function(name, option){
        dataPost[name] = option;
    }

    $scope.showAlert = function(title, template) {
        var alertPopup = $ionicPopup.alert({
            title: title,
            template: template
        });
     
        alertPopup.then(function(res) {
            $scope.goBack();
        });
    };

    $scope.goBack = function(){
        $ionicHistory.goBack();
    }

    $scope.getListProjet();

});