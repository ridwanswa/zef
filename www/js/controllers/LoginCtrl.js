﻿var dataUser	= app.value.UserData;
var dataImg		= app.value.ImgData;
var dataToken	= app.value.key;
var db	= null;
var config;
var configapi;
var osplatform;
var uuid;



app.controller('LoginCtrl', function($scope, $timeout, $stateParams, $ionicPopup, $ionicLoading, $rootScope,
	$state, $http, $cordovaSQLite, $ionicHistory, $ionicPlatform, $location, $ionicSideMenuDelegate, $cordovaDevice) {

	/* Call logout function */
	$scope.loggingOut = function() {
		$rootScope.$emit("callLogout", {});
	}

	/* Hiding menu if layer is level 2 */
	$ionicSideMenuDelegate.canDragContent(false);

	$ionicHistory.clearCache();

	/* Function for button back */
	$ionicPlatform.registerBackButtonAction(function() {
		if ($location.path() === "/login" || $location.path() === "login") {
			ionic.Platform.exitApp();
		} else if ($location.path() === "/dashboard" || $location.path() === "dashboard") {
			ionic.Platform.exitApp();
		} else {
			$ionicHistory.goBack();
		}
	}, 100);

	$scope.loginZef = function(){
		var dataPost = {
			'username' : 'admin_am', 
			'password' : 'admin'
		}
		var configapi = {
			headers	: {
				'Accept'			: '*/*',
				'Content-Type'		: 'application/x-www-form-urlencoded'
			}
		};
		$http.post(BASE_URL+'auth/login', dataPost, configapi).success(function(res){
			console.log(res);
		})
	}

})
