// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
// var app = angular.module('starter', ['ionic', 'ngCordova', 'ion-place-tools'])
var app = angular.module('starter', ['ionic', 'ngCordova', 'ngTextTruncate'])

app.run(function($ionicPlatform, $ionicPopup, $state, $http) {
  $ionicPlatform.ready(function() {

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }


	/* checking connection internet */
	if(window.Connection) {
		if(navigator.connection.type == Connection.NONE) {
			$ionicPopup.confirm({
				title: "No Internet Connection",
				content: "Sorry, no Internet connectivity detected. Please reconnect and try again.",
				buttons: [
							{ text: 'Ok', onTap: function(e) { return true; } },
						 ]
			})
			.then(function(result) {
				if(!result) {
					ionic.Platform.exitApp();
				}
			});
		}
	}
  });
})


app.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
	$ionicConfigProvider.backButton.previousTitleText(false);
	$ionicConfigProvider.tabs.style('standard');
	$ionicConfigProvider.backButton.text('');
	$ionicConfigProvider.views.swipeBackEnabled(false);
  $stateProvider

	.state('login', {
		cache : false,
		url: "/login",
		templateUrl: "templates/login.html",
		controller: 'LoginCtrl'
	})

  
	.state('dashboard', {
		cache : false,
		url: "/dashboard",
		templateUrl: "templates/dashboard.html",
		controller: 'DashboardCtrl'
	})

	.state('penitikanmc', {
		cache : false,
		url: "/penitikanmc",
		templateUrl: "templates/penitikanmc.html",
		controller: 'PenitikanMcCtrl'
	})

	.state('pengambilanfoto', {
		cache : false,
		url: "/pengambilanfoto",
		templateUrl: "templates/pengambilanfoto.html",
		controller: 'PengambilanFotoCtrl'
	})

	.state('pemindahanmc', {
		cache : false,
		url: "/pemindahanmc",
		templateUrl: "templates/pemindahanmc.html",
		controller: 'PemindahanMcCtrl'
	})

	.state('laporanharian', {
		cache : false,
		url: "/laporanharian",
		templateUrl: "templates/laporanharian.html",
		controller: 'LaporanHarianCtrl'
	})

	.state('laporanbarang', {
		cache : false,
		url: "/laporanbarang",
		templateUrl: "templates/laporanbarang.html",
		controller: 'LaporanBarangCtrl'
	})

	.state('status', {
		cache : false,
		url: "/status",
		templateUrl: "templates/status.html",
		controller: 'StatusCtrl'
	})
	
;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');
});
